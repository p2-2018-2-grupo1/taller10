procesador: base.c
	gcc -Wall $< -lpng -g -pthread -o $@

.PHONY: clean
clean:
	rm procesador
